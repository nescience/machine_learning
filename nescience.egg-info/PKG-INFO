Metadata-Version: 2.1
Name: nescience
Version: 1.0
Summary: Machine learning utilities based on the minimum nescience principle
Home-page: https://github.com/rleiva/nescience
Author: R. A. Garcia Leiva
Author-email: rgarcialeiva@gmail.com
License: UNKNOWN
Description: # Machine learning algorithms based on the minimum nescience principle
        
        ## NescienceDecisionTreeClassifier
        
        Constructing optimal decision trees without overfitting. On average, the size of the trees produced is much smaller that the trees generated with other algorithms, like for example CART.
        
        The algorithm generates binary trees for the classification of categorical response variables based on numerical or ordinal features. The classifier is fully compatible with the sckikit-learn library.
        
        ### Installation
        
        Download the Nescience directory. Make sure you run your script in the same directory where the Nescience subdirectory is located. Alternatively, you can put the Nescience subdirectory in a directory included in your PATH.
        
        ### Example of usage
        
        ```python
        from Nescience.NescienceDecisionTreeClassifier import NescienceDecisionTreeClassifier
        
        from sklearn.datasets import load_breast_cancer
        from sklearn.model_selection import train_test_split
        
        data = load_breast_cancer()
        X_train, X_test, y_train, y_test = train_test_split(data.data, data.target, test_size=0.3)
        
        model = NescienceDecisionTreeClassifier()
        model.fit(X_train, y_train)
        model.score(X_test, y_test)
        ```
        
        ### Addtional information
        
        For more advanced usage examples, please refer to the jupyter-lab notebooks XXX, XXX and XXX.
        
        For a gentle introduction to how the algorithm works refer to the following blog posts XXX and XXX.
        
        For the details about the theory of nescience and its applications to artificial intelligence you can download the book http://www.mathematicsunknown.com/nescience.pdf for free.
        
Platform: UNKNOWN
Classifier: Programming Language :: Python :: 3
Classifier: License :: OSI Approved :: GPLv3
Classifier: Operating System :: OS Independent
Description-Content-Type: text/markdown
