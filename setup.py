import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
     name         = 'nescience',  
     version      = '0.3',
     scripts      = ['Nescience/Nescience.py'] ,
     author       = "R. A. Garcia Leiva",
     author_email = "rgarcialeiva@gmail.com",
     description  = "Auto machine learning library based on the minimum nescience principle",
     long_description = long_description,
     long_description_content_type="text/markdown",
     url         = "https://gitlab.com/nescience/machine_learning",
     packages    = setuptools.find_packages(),
     classifiers = [
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
         "Operating System :: OS Independent",
     ],
 )
