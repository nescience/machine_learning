"""

Binary trees for classification based on the minimum nescience principle

@author:    Rafael Garcia Leiva
@mail:      rgarcialeiva@gmail.com
@web:       http://www.mathematicsunknown.com/
@copyright: All rights reserved
@version:   1.0 (Oct 2018)

TODO:
    - Adapt to coding standards of scikit-learn
       * __init__ should not have a verbose parameter
       * implement get_params and set_params methods
       * self.root should be called self.root_
       * validate with check_estimator
       * use project_template
       * implement unit tests

Extend:
    - Allow categorical features
    - Allow missing data

"""

import math
import numpy  as np
import pandas as pd
import collections

# Compression algorithms
import bz2
import lzma
import zlib

from sklearn.base import BaseEstimator, ClassifierMixin

class NescienceDecisionTreeClassifier(BaseEstimator, ClassifierMixin):
    """
    The following internal attributes will be used
    
      * root (dict)       - the computed tree
      * X (np.array)      - explanatory attributes
      * y (np.array)      - target values
      * classes_          - the classification classes labels
      * n_classes         - number of classification classes
      * nodesList (list)  - list of nodes pending to growth
      * nescience (float) - the best computed nescience so far
      * miscod (list)     - miscoding of each feature
      * fast (bool)       - use a fast version of the algorithm
      * verbose (bool)    - print additional information

    Each node of the tree is a dictionary with the following format:
    
        * feature   - index of the column of the feature
        * value     - value for the split (feature <= value)
        * left      - left branch (dict with same structure)
                      None if it is a leaf node
        * _left     - cache for left
        * right     - right branch (dict with same structure)
                      None if it is a leaf node
        * _right    - cache for right
        * ldata     - indices of current rows for left branch 
                      None if it is an intermediate node
        * rdata     - indices of current rows for right branch
                      None if it is an intermediate node
        * lforecast - forecasted class for the letf side
        * rforecast - forecasted class for the right side
        
    """
    
    # Constants defintion
    
    INVALID_INACCURACY = -1    # If algorithm cannot be applied to this dataset
    INVALID_REDUNDANCY = -2    # Too small model


    def __init__(self, method="Harmonic", compressor="bz2", fast=False, verbose=False):
        """
        Initialization of the tree
    
          * method:  String. Select the method to compute the nescience of the
                     tree, valid values are "Euclid", "Harmonic", "Geometric",
                     "Arithmetic", "Product" and "Addition"
          * compressor: algorithm used to compress data, valid values include
                     "bz2", "lzma" and "zlib"
          * fast:    Boolean. In case of very lage datasets use a faster
                     (although more inaccurate) version of the algorithm
          * verbose: Boolean. If true, prints out additional information
        """
        
        self.method     = method
        self.compressor = compressor
        self.fast       = fast
        self.verbose    = verbose
        

    def fit(self, X, y, sample_weight=None):
        """
        Fit a model (a tree) given a dataset
    
        The input dataset has to be in the following format:
    
           * X = list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])
           * y = list([y1, ..., yn])
           * sample_weight : array-like, shape = [n_samples] or None
       
        Return the fitted model
        """
        
        # TODO: check the input parameters and init arguments

        self.X = np.array(X)
        self.classes_, self.y = np.unique(y, return_inverse=True)
        self.n_classes = self.classes_.shape[0]
        
        if sample_weight is not None:
            self.sample_weight = sample_weight
        else:
            self.sample_weight = np.zeros(self.X.shape[0])            

        self.nodesList  = list()
        
        # Compute the contribution of each feature to miscoding
        self._initmiscod()
            
        # Create the initial node
        
        indices        = np.arange(0, len(self.X), 1)
        self.root      = self._create_node(indices)
        self.nescience = self._nescience()
        self.nodesList.append(self.root)

        if self.nescience == self.INVALID_INACCURACY:
            # There is anything more we can do with this dataset
            # since current model is already nearly perfect
            print("WARNING: Invalid Nescience")
            if self.verbose:
                print(self._tree2str())
            return self
            
        if self.verbose:
            print("Miscoding: ", self._miscoding(), "Inaccuracy: ", self._inaccuracy(), "Redundancy: ", self._redundancy(), "Nescience: ", self._nescience())

        self._fit()

        # Print out the best nescience achieved
        if self.verbose:
            
            if self._nescience() == self.INVALID_INACCURACY:
                print("WARNING: Invalid Nescience")
            else:
                print("Final nescience: " + str(self._nescience()))

            print(self._tree2str())

        return self


    """
    Internal method to fit a model (a tree) given a dataset
    """
    def _fit(self):
                           
        # Meanwhile there are nodes to consider
        while (self.nodesList):
            
            # Find the best node to develop
                        
            best_nsc  = self.nescience
            best_node = 0
            best_side = ""
            
            for i in range(len(self.nodesList)):
            
                # Get current node
                node = self.nodesList[i]
            
                # Try to create a left node
                #  - if empty
                #  - has more than one point
                
                if node['left'] == None and len(node['ldata']) > 1:
                                        
                    # Create the node, or get it from the cache
                    if node['_left'] == None:
                        node['left']  = self._create_node(node['ldata'])
                        node['_left'] = node['left']
                    else:
                        node['left']  = node['_left']
                
                    # Check if the node was created
                    if node['left'] != None:
                        
                        nsc = self._nescience()
                        
                        if nsc == self.INVALID_INACCURACY:
                            # We cannot do anything more with this dataset
                            self.nescience = nsc
                            return
            
                        # Save data if nescience has been reduced                        
                        if nsc < best_nsc:                                
                            best_nsc  = nsc
                            best_node = i
                            best_side = "left"
                            left = node['left']
                            
                        # And remove the node
                        node['left'] = None

                # Try to create a right node
                #  - if empty
                #  - has more than one point
                
                if node['right'] == None and len(node['rdata']) > 1:

                    # Create the node, or get it from the cache
                    if node['_right'] == None:
                        node['right']  = self._create_node(node['rdata'])
                        node['_right'] = node['right']
                    else:                       
                        node['right']  = node['_right']
                
                    # Check if the node was created
                    if node['right'] != None:
                        
                        nsc = self._nescience()
                        
                        if nsc == self.INVALID_INACCURACY:
                            # We cannot do anything more with this dataset
                            self.nescience = nsc
                            return
                        
                        # Save data if nescience has been reduced                        
                        if nsc < best_nsc:                                
                            best_nsc  = nsc
                            best_node = i
                            best_side = "right"
                            right = node['right']
                            
                        # And remove the node
                        node['right'] = None
                                                    
            # -> end for
            
            # Stop the while loop if we failed to improve the tree
            if best_nsc >= self.nescience:
                break
            
            # Add the best node found
            
            node = self.nodesList[best_node]
            self.nescience = best_nsc
            
            if best_side == "left":
                node['left'] = left
                self.nodesList.append(node['left'])
                # Save space
                node['ldata'] = None
            else:
                node['right'] = right
                self.nodesList.append(node['right'])
                # Save space
                node['rdata'] = None
                
            # Check if we still have to take this node into account
            if node['left'] != None and node['right'] != None:
                del self.nodesList[best_node]
                
            if self.verbose:
                print("Miscoding: ", self._miscoding(), "Inaccuracy: ", self._inaccuracy(), "Redundancy: ", self._redundancy(), "Nescience: ", self._nescience())                    
                
        # -> end while
        
        return
            

    def predict(self, X):
        """
        Predict class given a dataset
    
          * X = list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])
    
        Return a list of classes predicted
        """
        
        # TODO: Check that we have a model trained
        
        y = list()
        
        # For each entry in the dataset
        for i in range(len(X)):
            y.append(self._forecast(self.root, X[i]))
                
        return y


    def predict_proba(self, X):
        """
        Predict the probability of being in a class given a dataset
    
          * X = list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])
      
        Return a list of probabilities. The order of the list match the order
        the internal attribut classes_
        """
        
        # TODO: Check that we have a model trained
        
        proba = list()
        
        # For each entry in the dataset
        for i in range(len(X)):
            my_list = self._proba(self.root, X[i])
            proba.append(my_list)
            
        return np.array(proba)


    def score(self, X, y):
        """
        Evaluate the performance of the current model given a test dataset

           * X = list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])
           * y = list([y1, ..., yn])
    
        Return one minus the mean error
        """
        
        # TODO: Check that we have a model trained
        
        error = 0

        # For each entry in the dataset
        for i in range(len(X)):
            
            est = self._forecast(self.root, X[i])
                    
            if est != y[i]:
                error = error + 1
        
        score = 1 - error / len(X)
        
        return score


    """
    Compute the contribution of each feature to miscoding
    
    TODO: Somehow we should penalize non-contributing features
    """
    def _initmiscod(self):
         
        self.miscod = list()

        for i in range(0, self.X.shape[1]):

            Pred = list(pd.cut(self.X[:,i], bins=100, labels=range(0, 100)))
            Resp = self.y
            Join =  list(zip(Pred, Resp))

            count_X  = collections.Counter(Pred)
            count_y  = collections.Counter(Resp)
            count_Xy = collections.Counter(Join)
    
            tot_X = self.X.shape[0]

            ldm_X  = 0
            for key in count_X.keys():
                ldm_X = ldm_X + count_X[key] * ( - np.log2(count_X[key] / tot_X))
    
            ldm_y  = 0
            for key in count_y.keys():
                ldm_y = ldm_y + count_y[key] * ( - np.log2(count_y[key] / len(self.y)))

            ldm_Xy  = 0
            for key in count_Xy.keys():
                ldm_Xy = ldm_Xy + count_Xy[key] * ( - np.log2(count_Xy[key] / len(self.y)))
       
            K_yX = ldm_Xy - ldm_X
            # K_Xy = ldm_Xy - ldm_y
    
            # TODO: It should be something like:
            # mscd = max(K_yX, K_Xy) / max(ldm_X, ldm_y) / self.X.shape[1]
            
            mscd = K_yX / ldm_y / self.X.shape[1]

            self.miscod.append(mscd)
       
        return


    """
    Recursively forecast given a list of values for features
    
       * node - the current node being evaluated
       * values - a list of values used for forecasting
    
    Return the forecasted value
    """
    def _forecast(self, node, values):
        
        index = node['feature']
        value = node['value']

        if values[index] <= value:
            if node['left'] != None:
                y = self._forecast(node['left'], values)
            else:
                return node['lforecast']
        else:
            if node['right'] != None:
                y = self._forecast(node['right'], values)
            else:
                return node['rforecast']

        return y

    
    """
    Internal method to compute probabilties
    """
    def _proba(self, node, values):
        
        index = node['feature']
        value = node['value']

        if values[index] <= value:
            if node['left'] != None:
                prob_list = self._proba(node['left'], values)
            else:
                prob_list = list()                
                data = self.y[node['ldata']]
                length = len(data)
                for i in np.arange(self.n_classes):
                    prob_list.append(np.count_nonzero(data == i) / length)
                    
        else:
            if node['right'] != None:
                prob_list = self._proba(node['right'], values)
            else:
                prob_list = list()                
                data = self.y[node['rdata']]
                length = len(data)
                for i in np.arange(self.n_classes):
                    prob_list.append(np.count_nonzero(data == i) / length)

        return prob_list


    """
    Compute the forecast values for a node
        
     * ldata - indices of the left dataset
     * rdata - indices of the right dataset
     
    Return the forecasted values
    """
    def _compute_forecast(self, ldata, rdata):
        
        lly = list(self.y[ldata])
        lry = list(self.y[rdata])
        
        lval = max(set(lly), key=lly.count)
        rval = max(set(lry), key=lry.count)
                
        lforecast = self.classes_[lval]
        rforecast = self.classes_[rval]
        
        return lforecast, rforecast


    """
    Compute the entropy of a dataset
    
      * y - the dataset
      
    Return the entropy
    """
    def _entropy(self, y, weight):
        
        entropy = 0
        length  = len(y)
        
        if length == 0:    # Avoid dividing by 0
            return 0
        
        length = length + np.sum(weight)
        
        unique, indices, counts = np.unique(y, return_inverse=True, return_counts=True)
        
        for i in range(len(unique)):
        
            count = counts[i]
            count = count + np.sum(weight[np.where(indices == unique[i])])
                        
            probability = count / length
            information = 0    
            
            if probability != 0:    # Avoid computing the log2 of 0
                information = - probability * math.log2(probability)

            entropy = entropy + information

        return entropy

    
    """
    Compute the weighted entropy of a potential dataset split
    
      * index   - index of the attribute
      * value   - value of the attribute for the split
      * indices - indices of the rows to split
    
    Return the entropy
    """
    def _entropy_split(self, index, value, indices):
        
        mask = self.X[(indices),index] <= value  
        
        ldata   = self.y[indices][mask]
        lweight = self.sample_weight[indices][mask]
        
        rdata = self.y[indices][~mask]
        rweight = self.sample_weight[indices][~mask]
                            
        length_l = ldata.shape[0]
        length_r = rdata.shape[0]
        length_t = length_l + length_r
        
        entropy_l = self._entropy(ldata, lweight)
        entropy_r = self._entropy(rdata, rweight)
    
        entropy = (length_l / length_t) * entropy_l + (length_r / length_t) * entropy_r
            
        return entropy


    """
    Helper function to recursively compute the miscoding
    """
    def _miscodingcount(self, node):
                
        self.var_in_use[node['feature']] = 1
        
        # Process left branch
        if node['left'] != None:
            self._miscodingcount(node['left'])
    
        # Process right branch    
        if node['right'] != None:
            self._miscodingcount(node['right'])
                
        return

    
    """
    Compute the miscoding of the dataset used by the current model
      
    Return the miscoding
    """
    def _miscoding(self):
            
        self.var_in_use = np.zeros(self.X.shape[1])

        self._miscodingcount(self.root)

        miscoding = np.sum(np.multiply(self.var_in_use, self.miscod))
                    
        return 1 - miscoding   


    """
    Compute inaccuracy the current tree

    Warning: If the inaccuracy is a negative number it should not be used

    TODO: Perhaps I should add y[i] to the error string
      
    Return the inaccuracy
    """
    def _inaccuracy(self):
                        
        # Compute the list of errors
        error = list()
        for i in range(len(self.X)):
            pred = self._forecast(self.root, self.X[i])
            if pred != self.classes_[self.y[i]]:
                error.append(list(self.X[i]))

        # Compute the length of the encoding of the error
        error  = str(error).encode()
        
        if self.compressor == "lzma":
            dmodel = lzma.compress(error, preset=9)
        elif self.compressor == "zlib": 
            dmodel = zlib.compress(error, level=9)
        else: # By default use bz2   
            dmodel = bz2.compress(error, compresslevel=9)        
        
        ldm    = len(dmodel)
        
        # Check if the error is too small to compress
        if ldm >= len(error):
            return self.INVALID_INACCURACY

        # Compute the length of the encoding of the dataset
        data  = (str(self.X.tolist()) + str(self.y.tolist())).encode()
        data  = bz2.compress(data, compresslevel=9)
        ld    = len(data)
                
        # Inaccuracy = l(d/m) / l(d)
        inaccuracy = ldm / ld
        
        return inaccuracy


    """
    Compute the redundancy of the current tree
    
    Warning: If the redundancy is a negative number it should not be used
         
    Return the redundancy of the tree
    """
    def _redundancy(self):
    
        # Compute the model string and its compressed version
        model = self._tree2str().encode()
        
        if self.compressor == "lzma":
            compressed = lzma.compress(model, preset=9)
        elif self.compressor == "zlib": 
            compressed = zlib.compress(model, level=9)
        else: # By default use bz2   
            compressed = bz2.compress(model, compresslevel=9)
        
        # Check if the model is too small to compress
        if len(compressed) > len(model):
            return self.INVALID_REDUNDANCY
        
        # redundancy = 1 - l(m*) / l(m)
        redundancy = 1 - len(compressed) / len(model)
    
        return redundancy


    """
    Compute the nescience of a tree,
    using the method specified by the user
    
    Warning: If the nescience is a negative number it should not be used
          
    Return nescience
    """
    def _nescience(self):

        miscoding  = self._miscoding()
        redundancy = self._redundancy()
        inaccuracy = self._inaccuracy()

        if inaccuracy == self.INVALID_INACCURACY:
            # The inaccuracy is too small, there is anything
            # we can do with this dataset
            return self.INVALID_INACCURACY

        if redundancy < inaccuracy:
            # The model is still too small to compute the nescience
            # use innacuracy instead
            redundancy = 1
    
        # Compute the nescience according to the method specified by the user
        if self.method == "Euclid":
            # Euclidean distance
            nescience = math.sqrt(miscoding**2 + inaccuracy**2 + redundancy**2)
        elif self.method == "Arithmetic":
            # Arithmetic mean
            nescience = (miscoding + inaccuracy + redundancy) / 3
        elif self.method == "Geometric":
            # Geometric mean
            nescience = np.pow(miscoding * inaccuracy * redundancy, 1/3)
        elif self.method == "Product":
            # The product of both quantities
            nescience = miscoding * inaccuracy * redundancy
        elif self.method == "Addition":
            # The product of both quantities
            nescience = miscoding + inaccuracy + redundancy            
        else:
            # By default use the Harmonic mean
            if inaccuracy == 0:
                # Avoid dividing by zero
                inaccuracy = np.finfo(np.float32).tiny 
            nescience = 3 / ( (1/miscoding) + (1/inaccuracy) + (1/redundancy))            
    
        return nescience


    """
    Split a dataset based on an attribute and an attribute value
    
      * attribute   - column number of the attribute
      * value       - value of the attribute for the split
      * indices     - array with the indices of the rows to split
    
    Return
    
      * ldata - numpy array with those indices smaller or equal than
      * rdata - numpy array with those indices greater than
    """
    def _split_data(self, attribute, value, indices):
            
        lindex = np.where(self.X[:,attribute] <= value)
        rindex = np.where(self.X[:,attribute] > value)
        
        ldata = np.intersect1d(indices, lindex)
        rdata = np.intersect1d(indices, rindex)
        
        return ldata, rdata


    """
    Create a new tree node based on the best split point for the given dataset
    The split criteria is weighted entropy
    
      * indices - indices of the rows to split
    
    Return a full node
    """   
    def _create_node(self, indices):  
                     
        # Best values
        b_feature  = None
        b_value    = None
        b_entropy  = self.n_classes

        # Do not split if all the points belong to the same category
        if len(set(self.y[indices])) == 1:
            return None

        # For all features  
        for feature in range(0, len(self.X[0])):
                                                
            if self.fast == True:
                
                # Use percentiles instead of values,
                # intended for very large datasets

                values = list(self.X[indices][:,feature])
                
                if len(values) > 100:
                    values = np.percentile(values, range(0,100))
                
            else:

                # Some datasets repeat values many times
                # This trick can speedup the process of finding the best split
                values = set(self.X[indices][:,feature])
                values = list(values)
                            
                # We need the values sorted to compute middle points
                values.sort()

            # If we have only one value, we cannot split using this feature
            if len(values) <= 1:
                continue

            # For all known values
            for i in range(len(values)):
                            
                # Compute the entropy of splitting the dataset
                entropy = self._entropy_split(feature, values[i], indices)
                                                
                # And compare with the best split known
                if entropy < b_entropy:
                                    
                    # Compute the middle point between consecutive values
                                        
                    if i == len(values) - 1:
                        # avoid the last point
                        middle = values[i]
                    else:
                        middle = (values[i] + values[i+1]) / 2
                    
                    b_feature  = feature
                    b_value    = middle
                    b_entropy  = entropy

        # Safety check
        if b_feature == None:
            return None
                        
        # Create the new node
        ldata, rdata = self._split_data(b_feature, b_value, indices)
        
        # If all the data in one side, do not create the node
        if len(ldata) == 0 or len(rdata) == 0:
            return None
        
        lforecast, rforecast = self._compute_forecast(ldata, rdata)
                    
        my_dict = {'feature':   b_feature,
                   'value':     b_value,
                   'left':      None,
                   'right':     None,
                   '_left':     None,
                   '_right':    None,                   
                   'ldata':     ldata,
                   'rdata':     rdata,
                   'lforecast': lforecast,
                   'rforecast': rforecast}
        
        return my_dict


    """
    Helper function to recursively compute the head of the algorithm
    """
    def _head2str(self, node):

        myset = set()

        # Print the feature to take at this level
        myset.add('X%d' % (node['feature']+1))

        # Process left branch
        if node['left'] != None:
            myset = myset.union(self._head2str(node['left']))
    
        # Process right branch    
        if node['right'] != None:
            myset = myset.union(self._head2str(node['right']))
                
        return myset


    """
    Helper function to recursively compute the body of the algorithm
    """
    def _body2str(self, node, depth):

        string = ""

        # Print the decision to take at this level
    
        string = string + '%sif X%d <= %.3f:\n' % (' '*depth*4, (node['feature']+1), node['value'])

        # Process left branch
    
        if node['left'] == None:
            string = string + '%sreturn %s\n' % (' '*(depth+1)*4, node['lforecast'])
        else:
            string = string + self._body2str(node['left'],  depth+1)
    
        # Process right branch

        string = string + '%selse:\n' % (' '*depth*4)
    
        if node['right'] == None:
            string = string + '%sreturn %s\n' % (' '*(depth+1)*4, node['rforecast'])
        else:
            string = string + self._body2str(node['right'], depth+1)
                
        return string

    
    """
    Convert a tree into a string
    
    Convert a decision tree into a string using a austere representation
    Intended to compute the nescience of the tree
    although it can be used for visualization purposes as well
        
    Return a string with a representation of the tree
    """
    def _tree2str(self):
    
        string = "def tree" + str(self._head2str(self.root)) + ":\n"

        # Compute the function body
        string = string + self._body2str(self.root, 1)

        return string


    """
    Helper function to recursively compute the number of nodes
    """
    def _bodycount(self, node):

        nodes = 1
        
        # Process left branch
    
        if node['left'] == None:
            nodes = nodes + 1
        else:
            nodes = nodes + self._bodycount(node['left'])
    
        # Process right branch
    
        if node['right'] == None:
            nodes = nodes + 1
        else:
            nodes = nodes + self._bodycount(node['right'])
                
        return nodes

    
    """
    Helper function to count the number of nodes of a tree
    """
    def _nodecount(self):
    
        nodes = 0

        # Compute the function body
        nodes = nodes + self._bodycount(self.root)

        return nodes
    
    
    """
    Helper function to recursively compute the maximum depth of a tree
    """
    def _bodydepth(self, node):
        
        ldepth = rdepth = 0
        
        if node['left'] != None:
            ldepth = self._bodydepth(node['left'])
        
        if node['right'] != None:
            rdepth = self._bodydepth(node['right'])

        if ldepth > rdepth:
            return ldepth + 1
        else:
            return rdepth + 1


    """
    Helper function to count the maximun depth of a tree
    """
    def _maxdepth(self):
    
        return(self._bodydepth(self.root))
