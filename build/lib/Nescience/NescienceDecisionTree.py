"""

Binary trees for the classification of two classes (0 or 1)
given a set of numerical features
Tree building is based on the minimum nescience principle

@author:    Rafael Garcia Leiva
@mail:      rgarcialeiva@gmail.com
@web:       http://www.mathematicsunknown.com/
@copyright: All rights reserved
@version:   0.3 (17 Apr, 2018)

TODO:
    - Adapt to coding standards of scikit-learn
    - Find the best way to clean up redundant nodes

Extend:
    - Fit model to multiple categories
    - Allow categorical features
    - Allow missing values
    - Extend to regression models

"""

import math
import bz2
import numpy as np

from sklearn.base import BaseEstimator, ClassifierMixin

class NescienceDecisionTree(BaseEstimator, ClassifierMixin):

    """
    The following internal attributes will be used
    
      * root (dict)       - the computed tree
      * X (np.array)      - explanatory variables
      * y (np.array)      - target values
      * nodesList (list)  - list of nodes pending to growth
      * nescience (float) - the best computed nescience so far
      * bins (bool)       - use percentiles instead of real values
      * verbose (bool)    - print additional information

    Each node of the tree is a dictionary with the following format:
    
        * feature   - index of the column of the feature
        * value     - value for the split (feature <= value)
        * left      - left branch (dict with same structure)
                      None if it is a leaf node
        * right     - right branch (dict with same structure)
                      None if it is a leaf node
        * lX, ly    - current data for left branch 
                      None if it is an intermediate node
        * rX, ry    - current data for right branch
                      None if it is an intermediate node
        * lforecast - forecasted class for the letf side
        * rforecast - forecasted class for the right side
        
    """
    
    # Constants defintion
    
    INVALID_NESCIENCE = -1    # If algorithm cannot be applied to this dataset
        
    """
    Initialization of the tree
    
      * method:  String. Select the method to compute the nescience of the
                 tree, valid values are "Euclid", "Harmonic" and "Inner"
      * bins:    Boolean. In case of very lage dataset we could use
                 percentiles instead of values
      * verbose: Boolean. If true, prints out additional information
    """
    def __init__(self, method="Euclid", bins=False, verbose=False):

        # TODO: check the input parameters
        
        self.method  = method
        self.bins    = bins
        self.verbose = verbose
        
        # TODO: I do not know why we need this for BaggingClassifier
        self.classes_ = ["0", "1"]
        self.n_classes = 2


    def fit(self, X, y):
        """
        Fit a model (a tree) given a dataset
    
        The input dataset has to be in the following format:
    
           * X = list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])
           * y = list([y1, ..., yn])
       
        Return the fitted model

        """
        
        # TODO: check the input parameters

        self.nodesList  = list()
        self.X = np.array(X)
        self.y = np.array(y)
        
        # Create the initial node
        
        self.root      = self._create_node(self.X, self.y)
        self.nescience = self._nescience()
        self.nodesList.append(self.root)

        if self.nescience == self.INVALID_NESCIENCE:
            # There is anything more we can do with this dataset
            # since current model is already nearly perfect
            print("WARNING: Invalid Nescience")
            if self.verbose:
                print(self._tree2str())
            return self
            
        if self.verbose:
            print("Redundancy: ", self._redundancy(), "Inaccuracy: ", self._inaccuracy(), "Nescience: ", self._nescience())
                        
        # Meanwhile there are new nodes to consider
        while (self.nodesList):
            
            # Find the best node to develop
            
            best_nsc  = self.nescience
            best_node = 0
            best_side = ""
            
            for i in range(len(self.nodesList)):
            
                # Get current node
                node = self.nodesList[i]
            
                # Try to create a left node (if empty)
                
                if node['left'] == None:
            
                    node['left'] = self._create_node(node['lX'], node['ly'])
                
                    # Check if the node was created
                    if node['left'] != None:
                    
                        nsc = self._nescience()
            
                        # Save data if nescience has been reduced                        
                        if nsc < best_nsc:                                
                            best_nsc  = nsc
                            best_node = i
                            best_side = "left"
                            left = node['left']
                            
                            # And remove the new node
                            node['left'] = None

                # Try to create a right node (if empty)
                
                if node['right'] == None:
            
                    node['right'] = self._create_node(node['rX'], node['ry'])
                
                    # Check if the node was created
                    if node['right'] != None:
                    
                        nsc = self._nescience()
                    
                        # Save data if nescience has been reduced                        
                        if nsc < best_nsc:                                
                            best_nsc  = nsc
                            best_node = i
                            best_side = "rigth"
                            right = node['right']
                            
                            # And remove the new node
                            node['right'] = None
                            
            # -> end for
            
            # Stop if we couldn't improve the tree
            
            if best_nsc >= self.nescience:
                break
            
            # Add the best node found
            
            node = self.nodesList.pop(best_node)
            self.nescience = best_nsc
            
            if best_side == "left":
                node['left'] = left
                self.nodesList.append(node['left'])
                # Save space
                node['lX'] = None
                node['ly'] = None                
            else:
                node['right'] = right
                self.nodesList.append(node['right'])
                # Save space
                node['rX'] = None
                node['ry'] = None
                
            # Check if we still have to take this node into account
            if node['left'] == None or node['right'] == None:
                self.nodesList.append(node)
                
            if self.verbose:
                print("Redundancy: ", self._redundancy(), "Inaccuracy: ", self._inaccuracy(), "Nescience: ", self._nescience())            
            
        # -> end while
            
        # Print out the best nescience achieved
        if self.nescience == self.INVALID_NESCIENCE:
            print("WARNING: Invalid Nescience")
        else:
            print("Nescience: " + str(self.nescience))

        if self.verbose:
            print(self._tree2str())

                
        return self


    def predict(self, X):
        """
        Predict class 0 or 1 given a dataset
    
          * X = list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])
    
        Return a list of predictions
        """
        
        # TODO: Check that we have a model trained
        
        y = list()
        
        # For each entry in the dataset
        for i in range(len(X)):
            y.append(self._forecast(self.root, X[i]))
                
        return y


    def predict_proba(self, X):
        """
        Predict the probability of being in class 0 given a dataset
    
          * X = list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])

        TODO: Properly document
      
        Return a list of probabilities
        """
        
        # TODO: Check that we have a model trained
        
        proba = list()
        
        # For each entry in the dataset
        for i in range(len(X)):
            x, y = self._proba(self.root, X[i])
            proba.append([x, y])
            
        return np.array(proba)


    def score(self, X, y):
        """
        Evaluate the performance of the current model given a test dataset

           * X = list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])
           * y = list([y1, ..., yn])
    
        Return one minus the mean error
        """
        
        # TODO: Check that we have a model trained

        # TODO: This function should be based on self._error()
        
        error = 0

        # For each entry in the dataset
        for i in range(len(X)):
            
            est = self._forecast(self.root, X[i])
                    
            if est != y[i]:
                error = error + 1
        
        score = 1 - error / len(X)
        
        return score


    """
    Recursively forecast given a list of values for features
    
       * node - the current node being evaluated
       * values - a list of values used for forecasting
    
    Return the forecasted value
    """
    def _forecast(self, node, values):
        
        index = node['feature']
        value = node['value']

        if values[index] <= value:
            if node['left'] != None:
                y = self._forecast(node['left'], values)
            else:
                return node['lforecast']
        else:
            if node['right'] != None:
                y = self._forecast(node['right'], values)
            else:
                return node['rforecast']

        return y
    
    
    # TODO: Document
    # TODO: Optimize
    def _proba(self, node, values):
        
        index = node['feature']
        value = node['value']

        if values[index] <= value:
            if node['left'] != None:
                y = self._proba(node['left'], values)
            else:
                ones = np.sum(node['ly'])
                return 1 - ones/len(node['ly']), ones/len(node['ly'])
        else:
            if node['right'] != None:
                y = self._proba(node['right'], values)
            else:
                ones = np.sum(node['ry'])
                return 1 - ones/len(node['ry']), ones/len(node['ry'])

        return y


    """
    Compute the forecast values for a node
        
     * ly - the left dataset
     * ry - the right dataset
     
    Return the forecasted values
    """
    def _compute_forecast(self, ly, ry):
        
        lly = list(ly)
        lry = list(ry)
                
        lforecast = max(set(lly), key=lly.count)
        rforecast = max(set(lry), key=lry.count)
        
        # If both values are the same, use the most relevant
        if lforecast == rforecast:
            if len(lly) > len(lry):
                rforecast =  1 - lforecast
            else:
                lforecast =  1 - rforecast
                
        return lforecast, rforecast


    """
    Evaluate the absolute error of the current tree
    
    Return the absolute error
    """
    def _error(self):
                
        error = 0

        # For each entry in the dataset
        for i in range(len(self.X)):
            
            est = self._forecast(self.root, self.X[i])
                    
            if est != self.y[i]:
                error = error + 1

        return error


    """
    Compute the redundancy of the current tree
    
    Warning: If the redundancy is a negative number it should not be used
         
    Return the redundancy of the tree
    """
    def _redundancy(self):
    
        # Compute the model string and its compressed version
        model = self._tree2str().encode()
        compressed = bz2.compress(model, compresslevel=9)
        
        # redundancy = 1 - l(d*) / l(d)
        redundancy = 1 - len(compressed) / len(model)
    
        return redundancy


    """
    Compute the entropy of a dataset
    
      * y - the dataset
      
    Return the entropy
    """
    def _entropy(self, y):
        
        length = len(y)
        
        if length == 0:    # Avoid dividing by 0
            return 0
        
        # Compute the number of ones
        
        ones = np.sum(y)
                
        #
        # Compute the weigthed entropy
        #
        # p0 means the probability of being 0
        # i0 means the information of the 0's
        #
    
        p0 = (length - ones) / length
        p1 = 1 - p0
        
        i0 = 0            
        if p0 != 0:    # Avoid computing the log2 of 0
            i0 = - p0 * math.log2(p0)

        i1 = 0    
        if p1 != 0:
            i1 = - p1 * math.log2(p1)

        entropy = i0 + i1        
        
        return entropy

    
    """
    Compute the weighted entropy of a potential dataset split
    
      * index - index of the attribute
      * value - value of the attribute for the split
      * data  - dataset to split
    
    Return the entropy
    """
    def _entropy_split(self, index, value, X, y):
        
        # TODO: The implementation of this method is hihgly inefficient

        lX, ly, rX, ry = self._split_data(index, value, X, y)
                    
        length_l = len(ly)
        length_r = len(ry)
        length_t = len(y)

        entropy_l = self._entropy(ly)
        entropy_r = self._entropy(ry)
    
        entropy = (length_l / length_t) * entropy_l + (length_r / length_t) * entropy_r
            
        return entropy


    """
    Compute inaccuracy the current tree

    Warning: If the inaccuracy is a negative number it should not be used
      
    Return the inaccuracy
    """
    def _inaccuracy(self):
                        
        # Compute the list of errors
        error = list()
        for i in range(len(self.X)):
            pred = self._forecast(self.root, self.X[i])
            if pred != self.y[i]:
                error.append(list(self.X[i]))

        # Compute the length of the encoding of the error
        error  = str(error).encode()
        dmodel = bz2.compress(error, compresslevel=9)
        ldm    = len(dmodel)
        
        # Check if the error is too small to compress
        if ldm >= len(error):
            return -1

        # Compute the length of the encoding of the dataset
        data  = (str(self.X.tolist()) + str(self.y.tolist())).encode()
        data  = bz2.compress(data, compresslevel=9)
        ld    = len(data)
                
        # Inaccuracy = l(d/m) / l(d)
        inaccuracy = ldm / ld
        
        return inaccuracy


    """
    Compute the nescience of a tree,
    using the method specified by the user
    
    Warning: If the nescience is a negative number it should not be used
          
    Return nescience
    """
    def _nescience(self):

        redundancy = self._redundancy()
        inaccuracy = self._inaccuracy()
        
        if inaccuracy < 0:
            # The inaccuracy is too small, there is anything
            # we can do with this dataset
            return self.INVALID_NESCIENCE

        if redundancy < 0:
            # The model is too small, by using the maximum redundancy
            # nescience depends only on inaccuracy
            redundancy = 1
    
        # Compute the nescience according to the method specified by the user
        if self.method == "Euclid":
            # Euclidean distance
            nescience = math.sqrt(redundancy**2 + inaccuracy**2)
        elif self.method == "Harmonic":
            # Harmonic mean
            if inaccuracy == 0:
                # Avoid dividing by zero
                inaccuracy = np.finfo(np.float32).tiny 
            nescience = 1 / ( (1/redundancy) + (1/inaccuracy) )
        elif self.method == "Geometric":
            # Geometric mean
            if inaccuracy == 0:
                # Avoid dividing by zero
                inaccuracy = np.finfo(np.float32).tiny 
            nescience = np.sqrt(redundancy * inaccuracy)
        elif self.method == "Entropy":
            # Geometric mean
            if inaccuracy == 0:
                # Avoid dividing by zero
                inaccuracy = np.finfo(np.float32).tiny 
            nescience = - redundancy * np.log2(redundancy) - inaccuracy * np.log2(inaccuracy)                        
        else:
            # Inner product
            nescience = redundancy * inaccuracy
    
        return nescience


    """
    Split a dataset based on an attribute and an attribute value
    
      * index   - index of the attribute
      * value   - value of the attribute for the split
      * X, y    - dataset to split
    
    Return
    
      * lX, ly - numpy arrays with those vaalues smaller or equal than
      * rX, ry - numpy arrays with those those values greater than
    """
    def _split_data(self, index, value, X, y):
            
        condl = np.where(X[:,index] <= value)
        condr = np.where(X[:,index] > value)
        
        lX = X[condl]
        ly = y[condl]
        
        rX = X[condr]
        ry = y[condr]

        return lX, ly, rX, ry


    """
    Create a new tree node based on the best split point for the given dataset
    The split criteria is weighted entropy
    
      * data - dataset to split
    
    Return a full node
    """   
    def _create_node(self, X, y):
        
        # Best values
        b_feature  = None
        b_value    = None
        b_entropy  = 1

        # For all features
        for feature in range(0, len(X[0])):
                                    
            if self.bins == False:

                # Some datasets repeat values many times
                # This trick can speedup the process of finding the best split
                values = set(X[:,feature])
                values = list(values)
            
                # We need the values sorted to compute middle points
                values.sort()
                                
            else:

                # We can use percentiles instead of values,
                # intended for very large datasets
                # TODO: The number of bins shuld be a configurable parameter

                values = list(X[:,feature])
                values = np.percentile(values, range(0,100))
                
            # For all known values
            for i in range(len(values)):
                            
                # Compute the entropy of splitting the dataset
                entropy = self._entropy_split(feature, values[i], X, y)
                                
                # And compare with the best split known
                if entropy < b_entropy:
                                    
                    # Compute the middle point between consecutive values
                    if i == len(values) - 1:
                        # avoid the last point
                        middle = values[i]
                    else:
                        middle = (values[i] + values[i+1]) / 2
                    
                    b_feature  = feature
                    b_value    = middle
                    b_entropy  = entropy
    
        # Create the new node
        
        # TODO: If the best entropy is still 1, we should not create
        #       the node. Better say 'no suitable model has been found'
    
        lX, ly, rX, ry = self._split_data(b_feature, b_value, X, y)
        
        # If all the data in one side, do not create the node
        if len(lX) == 0 or len(rX) == 0:
            return None
        
        lforecast, rforecast = self._compute_forecast(ly, ry)
                    
        my_dict = {'feature':   b_feature,
                   'value':     b_value,
                   'left':      None,
                   'right':     None,
                   'lX':        lX,
                   'ly':        ly,
                   'rX':        rX,
                   'ry':        ry,
                   'lforecast': lforecast,
                   'rforecast': rforecast}
    
        return my_dict

    """
    Helper function to recursively compute the head of the algorithm
    """
    def _head2str(self, node):

        myset = set()

        # Print the feature to take at this level
        myset.add('X%d' % (node['feature']+1))

        # Process left branch
        if node['left'] != None:
            myset = myset.union(self._head2str(node['left']))
    
        # Process right branch    
        if node['right'] != None:
            myset = myset.union(self._head2str(node['right']))
                
        return myset


    """
    Helper function to recursively compute the body of the algorithm
    """
    def _body2str(self, node, depth):

        string = ""

        # Print the decision to take at this level
    
        string = string + '%sif X%d <= %.3f:\n' % (' '*depth*4, (node['feature']+1), node['value'])

        # Process left branch
    
        if node['left'] == None:
            string = string + '%sreturn %s\n' % (' '*(depth+1)*4, node['lforecast'])
        else:
            string = string + self._body2str(node['left'],  depth+1)
    
        # Process right branch

        string = string + '%selse:\n' % (' '*depth*4)
    
        if node['right'] == None:
            string = string + '%sreturn %s\n' % (' '*(depth+1)*4, node['rforecast'])
        else:
            string = string + self._body2str(node['right'], depth+1)
                
        return string

    
    """
    Convert a tree into a string
    
    Convert a decision tree into a string using a minimal expression
    Intended to compute the nescience of the tree
    although it can be used for visualization purposes as well
        
    Return a string with a representation of the tree
    """
    def _tree2str(self):
    
        string = "def tree" + str(self._head2str(self.root)) + ":\n"

        # Compute the function body
        string = string + self._body2str(self.root, 1)

        return string

