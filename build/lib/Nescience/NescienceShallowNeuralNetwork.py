"""

Building optimal shallow (one hidden layer) neural networks
based on the minimum nescience principle

@author: Rafael Garcia Leiva
@mail:   rgarcialeiva@gmail.com
@web:    http://www.mathematicsunknown.com/
@copyright: All rights reserved
@version: 0.1 (13 Feb, 2018)

Based on: Planar data classification with a hidden layer. Neural Networks and
          Deep Learning. By deeplearning.ai

TODO:

The following internal attributes will be used
    
    * n_h (int) - the number of hidden layers
    * n_iter
    * X
    * Y
    * params
    * verbose

'params' is a python dictionary with the following structure:
    W1 -- weight matrix of shape (n_h, n_x)
    b1 -- bias vector of shape (n_h, 1)
    W2 -- weight matrix of shape (n_y, n_h)
    b2 -- bias vector of shape (n_y, 1)
    
"""

import numpy as np

from sklearn.base import BaseEstimator, ClassifierMixin

class NescienceShallowNeuralNetwork(BaseEstimator, ClassifierMixin):

    def __init__(self, nhidden=4, niterations = 10000, verbose=False):
        """Initialize the class.
    
        Arguments:
        hidden  -- size of the hidden layer
        verbose -- prints extra information
        """
       
        # TODO: check the input parameters
        
        self.n_h     = nhidden
        self.n_iter  = niterations
        self.verbose = verbose


    def fit(self, X, Y):
        """Fit a model (shallow neural network) given a dataset.
        
        Arguments:
        X -- explanatory variables in the format "list([[x11, x12, x13, ...], ..., [xn1, xn2, ..., xnm]])"
        Y -- dependent variable in the format "list([y1, ..., yn])"
       
        Return the fitted model
        """
        
        # TODO: check the input parameters
        
        self.X = X
        self.Y = Y
    
        self.initialize_parameters()
    
        # Loop (gradient descent)
        for i in range(0, num_iterations):
         
            ### START CODE HERE ### (≈ 4 lines of code)
            # Forward propagation. Inputs: "X, parameters". Outputs: "A2, cache".
            A2, cache = forward_propagation(X, parameters)
        
            # Cost function. Inputs: "A2, Y, parameters". Outputs: "cost".
            cost = compute_cost(A2, Y, parameters)
 
            # Backpropagation. Inputs: "parameters, cache, X, Y". Outputs: "grads".
            grads = backward_propagation(parameters, cache, X, Y)
 
            # Gradient descent parameter update. Inputs: "parameters, grads". Outputs: "parameters".
            parameters = update_parameters(parameters, grads, learning_rate = 1.2)
        
            ### END CODE HERE ###
        
            # Print the cost every 1000 iterations
            if print_cost and i % 1000 == 0:
                print ("Cost after iteration %i: %f" %(i, cost))

        return parameters

        
        # We need to store the concatenated dataset
        
        self.dataset = list()
        
        for i in range(len(X)):
            row = list(X[i])
            row.append(y[i])
            self.dataset.append(row)

        # Create the initial node
        
        self.root       = self._create_node(self.dataset)
        self.nescience  = self._nescience()
        self.changed    = True
        self.depth      = 1

        if self.nescience < 0:
            # There is anything more we can do with this dataset
            print("Warning! Nescience: " + str(self.nescience))
            if self.verbose:
                print(self._tree2str())
            return self
            
        if self.verbose:
            print("Redundancy: ", self._redundancy(), "Inaccuracy: ", self._inaccuracy(), "Nescience: ", self._nescience())
            
        # Meanwhile the tree keeps growing
        while (self.changed):
            
            self.changed = False

            # Build the next level of the tree (recursive method, start with 1)
            self._build_tree(self.root, 1)
            
            # Tree building must be shallow, and so, during each iteration
            # only one level more can be added
            self.depth = self.depth + 1
            
            if self.verbose:
                print("Redundancy: ", self._redundancy(), "Inaccuracy: ", self._inaccuracy(), "Nescience: ", self._nescience())

        # Print out the best nescience achieved
        if self.nescience < 0:
            print("Warning! Nescience: " + str(self.nescience))
        else:
            print("Nescience: " + str(self.nescience))
        
        if self.verbose:
            print(self._tree2str())
        
        return self        


    """
    Initialize the internal attribute 'self.parameters'
    
    W1 and W2 are initialized using small random numbers
    b1 and b2 are intiialized to zero
    """
    def _initialize_parameters(self):
    
        W1 = np.random.randn(self.n_h, self.X.shape[0]) * 0.01
        b1 = np.zeros((self.n_h, 1))
        W2 = np.random.randn(self.Y.shape[0], self.n_h) * 0.01
        b2 = np.zeros((self.Y.shape[0], 1))
    
        assert (W1.shape == (self.n_h, self.X.shape[0]))
        assert (b1.shape == (self.n_h, 1))
        assert (W2.shape == (self.Y.shape[0], self.n_h))
        assert (b2.shape == (self.Y.shape[0], 1))
    
        self.parameters = {"W1": W1,
                           "b1": b1,
                           "W2": W2,
                           "b2": b2}
    

    def forward_propagation(X, parameters):
        """
        Argument:
        X -- input data of size (n_x, m)
        parameters -- python dictionary containing your parameters (output of initialization function)
    
        Returns:
        A2 -- The sigmoid output of the second activation
        cache -- a dictionary containing "Z1", "A1", "Z2" and "A2"
        """
        # Retrieve each parameter from the dictionary "parameters"
        ### START CODE HERE ### (≈ 4 lines of code)
        W1 = parameters['W1']
        b1 = parameters['b1']
        W2 = parameters['W2']
        b2 = parameters['b2']
        ### END CODE HERE ###
    
        # Implement Forward Propagation to calculate A2 (probabilities)
        ### START CODE HERE ### (≈ 4 lines of code)
        Z1 = np.matmul(W1, X) + b1
        A1 = np.tanh(Z1)
        Z2 = np.matmul(W2, A1) + b2
        A2 = sigmoid(Z2)
        ### END CODE HERE ###
    
        assert(A2.shape == (1, X.shape[1]))
    
        cache = {"Z1": Z1,
                 "A1": A1,
                 "Z2": Z2,
                 "A2": A2}
    
        return A2, cache

    def compute_cost(A2, Y, parameters):
        """
        Computes the cross-entropy cost given in equation (13)
    
        Arguments:
        A2 -- The sigmoid output of the second activation, of shape (1, number of examples)
        Y -- "true" labels vector of shape (1, number of examples)
        parameters -- python dictionary containing your parameters W1, b1, W2 and b2
    
        Returns:
        cost -- cross-entropy cost given equation (13)
        """
    
        m = Y.shape[1] # number of example

        # Compute the cross-entropy cost
        ### START CODE HERE ### (≈ 2 lines of code)
        logprobs = np.multiply(np.log(A2),Y)
        cost     = - np.sum(logprobs)
        ### END CODE HERE ###
    
        cost = np.squeeze(cost)     # makes sure cost is the dimension we expect. 
                                    # E.g., turns [[17]] into 17 
        assert(isinstance(cost, float))
    
        return cost

    def backward_propagation(parameters, cache, X, Y):
        """
        Implement the backward propagation using the instructions above.
    
        Arguments:
        parameters -- python dictionary containing our parameters 
        cache -- a dictionary containing "Z1", "A1", "Z2" and "A2".
        X -- input data of shape (2, number of examples)
        Y -- "true" labels vector of shape (1, number of examples)
    
        Returns:
        grads -- python dictionary containing your gradients with respect to different parameters
        """
        m = X.shape[1]
    
        # First, retrieve W1 and W2 from the dictionary "parameters".
        ### START CODE HERE ### (≈ 2 lines of code)
        W1 = parameters['W1']
        W2 = parameters['W2']
        ### END CODE HERE ###
        
        # Retrieve also A1 and A2 from dictionary "cache".
        ### START CODE HERE ### (≈ 2 lines of code)
        A1 = cache['A1']
        A2 = cache['A2']
        ### END CODE HERE ###
    
        # Backward propagation: calculate dW1, db1, dW2, db2. 
        ### START CODE HERE ### (≈ 6 lines of code, corresponding to 6 equations on slide above)
        dZ2 = A2 - Y
        dW2 = (1 / Y.shape[1]) * np.dot(dZ2, A1.T)
        db2 = (1 / Y.shape[1]) * np.sum(dZ2, axis=1, keepdims=True)
        dZ1 = np.matmul(W2.T, dZ2) * (1 - np.power(A1, 2))
        dW1 = (1 / Y.shape[1]) * np.dot(dZ1, X.T)
        db1 = (1 / Y.shape[1]) * np.sum(dZ1, axis=1, keepdims=True)
        ### END CODE HERE ###
    
        grads = {"dW1": dW1,
                 "db1": db1,
                 "dW2": dW2,
                 "db2": db2}
    
        return grads

    def update_parameters(parameters, grads, learning_rate = 1.2):
        """
        Updates parameters using the gradient descent update rule given above
    
        Arguments:
        parameters -- python dictionary containing your parameters 
        grads -- python dictionary containing your gradients 
    
        Returns:
        parameters -- python dictionary containing your updated parameters 
        """
        # Retrieve each parameter from the dictionary "parameters"
        ### START CODE HERE ### (≈ 4 lines of code)
        W1 = parameters['W1']
        b1 = parameters['b1']
        W2 = parameters['W2']
        b2 = parameters['b2']
        ### END CODE HERE ###
    
        # Retrieve each gradient from the dictionary "grads"
        ### START CODE HERE ### (≈ 4 lines of code)
        dW1 = grads['dW1']
        db1 = grads['db1']
        dW2 = grads['dW2']
        db2 = grads['db2']
        ## END CODE HERE ###
    
        # Update rule for each parameter
        ### START CODE HERE ### (≈ 4 lines of code)
        W1 = W1 - learning_rate * dW1
        b1 = b1 - learning_rate * db1
        W2 = W2 - learning_rate * dW2
        b2 = b2 - learning_rate * db2
        ### END CODE HERE ###
    
        parameters = {"W1": W1,
                      "b1": b1,
                      "W2": W2,
                      "b2": b2}
    
        return parameters

    def predict(parameters, X):
        """
        Using the learned parameters, predicts a class for each example in X
    
        Arguments:
        parameters -- python dictionary containing your parameters 
        X -- input data of size (n_x, m)
    
        Returns
        predictions -- vector of predictions of our model (red: 0 / blue: 1)
        """
    
        # Computes probabilities using forward propagation, and classifies to 0/1 using 0.5 as the threshold.
        ### START CODE HERE ### (≈ 2 lines of code)
        A2, cache = forward_propagation(X, parameters)
        predictions = A2 > 0.5
        ### END CODE HERE ###
    
        return predictions